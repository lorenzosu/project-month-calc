/* Requires the moment JS library: https://momentjs.com/ */

function calculateMonthsInterval(start_date_string, end_date_string) {
    start = moment(start_date_string, "DD MMMM YYYY");
    end = moment(end_date_string, "DD MMMM YYYY");
    delta = moment.duration(end.diff(start));
    return(Math.floor(delta.asMonths()) + 1);
  }

function calculateEndMonthDate(start_date_string, month_delta) {
  start = moment(start_date_string, "DD MMMM YYYY");
  end = start.add(month_delta, "M")
  return(end);
}
