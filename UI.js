/* Requires the moment JS library: https://momentjs.com/ */

function displayMonth(){
    console.log("Calculating month...")
    var start_month = $("#start-month-select").val();
    var start_year = $("#start-year-select").val();
    var date1 = "01" + " " + start_month + " " + start_year;
    
    var now_month = $("#now-month-select").val();
    var now_year = $("#now-year-select").val();
    var date2 = 28 + " "+ now_month + " " + now_year;

    var result = calculateMonthsInterval(date1, date2);
    $( "#now-month" ).val(result);
    highlightBackground("now-month", "highlight")
    if(result < 0) {
      var template = `\"To\" date is before start (negative month: ${result})`;
      errorAlert("errorAlert", template, SHOW_CLASS, HIDE_CLASS);
      console.warn("Month < 0!");
    }
    if(result > MAX_MONTHS) {
      var template = `\"To\" date results in month beyond ${MAX_MONTHS}! (anyway would be month ${result})`;
      errorAlert("errorAlert", template, SHOW_CLASS, HIDE_CLASS);
      console.warn("Result > max months (" + MAX_MONTHS + ")");
    }
    console.log("|- month: " + result);
}

function displayEndDate() {
  console.log("Change month. Calculate new now date...")
  var start_month = $("#start-month-select").val();
  var start_year = $("#start-year-select").val();
  var date1 = "01" + " " + start_month + " " + start_year;
  
  var newMonth = $( "#now-month").val() - 1;
  var newEndDate = calculateEndMonthDate(date1, newMonth);
  var newMonth = newEndDate.format("MMMM");
  var newYear = newEndDate.format("YYYY");

  $(" #now-month-select ").val(newMonth);
  $(" #now-year-select ").val(newYear);
  highlightBackground("now-month-select", "highlight")
  highlightBackground("now-year-select", "highlight")
  if(newYear > MAX_YEAR)
  {
    var template = `result year is > than maximum year (result: ${newYear})`
    errorAlert("errorAlert", template, SHOW_CLASS, HIDE_CLASS);


  }
  console.log("New now date... " + newMonth + " " + newYear);
}

/* (re)set 'now' date to today */
function resetTodayForm(month_id, year_id) {
  var today = moment();
  $( year_id ).val(today.year());
  $( month_id ).val(today.format("MMMM"));
}

function highlightBackground(elementID, className) {
  $("#" + elementID).addClass(className);
  setTimeout(function(){
      $("#" + elementID).removeClass(className);
    }, 1000);
}

function errorAlert(alertID, alertText, showClass, hideClass) {
  $("#" + alertID).removeClass(hideClass);
  $("#errorText").text(alertText);
  console.log(HIDE_CLASS);
}

function hideAlert(alertID, showClass, hideClass) {
  $("#errorText").text('');
  $("#" + alertID).removeClass(showClass).addClass(hideClass);
}

/* Callbacks for changes changes */
$( "#moth_selector" ).change(function() {
  hideAlert("errorAlert", SHOW_CLASS, HIDE_CLASS);
  displayMonth();
});

$( "#now-month").change(function() {
  hideAlert("errorAlert", SHOW_CLASS, HIDE_CLASS);
  displayEndDate();
});

$( "#now-reset").click(function() { 
  hideAlert("errorAlert", SHOW_CLASS, HIDE_CLASS);

  resetTodayForm("#now-month-select", "#now-year-select");
  displayMonth();
});

$("#closeAlert").click(function() {
  hideAlert("errorAlert", SHOW_CLASS, HIDE_CLASS);
});

/* Main scope */
/* Globals */
MONTH_LIST = moment.months();
TODAY = moment();
MAX_MONTHS = 60;

// Global show and hide classes e.g. for the alert
SHOW_CLASS = "d-inline";
HIDE_CLASS = "d-none";


/* populate options for #start-month-select and #now-month-select */
for (i = 0; i < 12; i++) {
  var month_name = MONTH_LIST[i];
  var display_month = month_name;
    var month_template = `<option value=${month_name}>${display_month}</option>`;
  $("#start-month-select").append(month_template);
  $("#now-month-select").append(month_template);}

for (i = 1; i <= MAX_MONTHS; i++) {
  this_month_template = `<option value=${i}>${i}</option>`;
  $( "#now-month" ).append(this_month_template);
}

/* populate year options with +/- 10 years from noe */
var year_delta = 10;
var MAX_YEAR = TODAY.year() + year_delta;
for (i = TODAY.year() - year_delta; i < MAX_YEAR; i++) { 
  year_template = `<option value=${i}>${i}</option>`;
  $("#start-year-select").append(year_template);
  $("#now-year-select").append(year_template);
}

/* Initialize selected values for months and years resepctively */
$("#start-year-select").val(TODAY.year() - 1);

// TODAY will now change...
$("#start-month-select").val(TODAY.add(1, 'month').format("MMMM"));

resetTodayForm("#now-month-select", "#now-year-select");
displayMonth();
