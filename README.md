Project month calculator made in HTML / JavaScript / Bootstrap

Particularly useful for calculating the months of EC funded projects like H2020, Horizon Europe, etc.

All UI controls are 'reactive' (i.e. change any control and the others will also change accordingly).

I first made  this around 2018 when I was very full with different EU projects all with different start dates and durations and many parallel deadlines. I first made a spreadsheet with similar functionality but a) it was ugly; b) it was hard to share with team-mates.

After releasing it here on GitLab, team-mates and myself have been using this a lot especially when managing multple EU projects. It has also been 'embedded' as a single page in certain tools which allow to embed external web pages. ;- )

Try it here: https://lorenzosu.gitlab.io/project-month-calc/

*Requirements*
Moment.js library: https://momentjs.com/